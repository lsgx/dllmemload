#include "CppUnitTest.h"

#include "testmemload.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace mldll_mtest
{
    TEST_CLASS(TestMemLoad)
    {
    public:
        TEST_METHOD(CaseLoadFromFile)
        {
            Logger::WriteMessage("CaseLoadFromFile ......");
            Assert::AreEqual(LoadFromFile(), 0);
        }

        TEST_METHOD(CaseLoadFromMemory)
        {
            Logger::WriteMessage("CaseLoadFromMemory ......");
            Assert::AreEqual(LoadFromMemory(), 0);
        }

        TEST_METHOD(CaseTestCustomAllocAndFree)
        {
            Logger::WriteMessage("CaseTestCustomAllocAndFree ......");
            Assert::AreEqual(TestCustomAllocAndFree(), 0);
        }
    };
}
